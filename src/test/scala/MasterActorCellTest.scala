import actors.MasterActor.{StartDimension, StateReceived}
import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActors, TestKit}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class MasterActorCellTest() extends TestKit(ActorSystem("MySpec")) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "A MasterActor" must {
    "receive a StartDimension with matrix dimension as parameter" in {
      val echo = system.actorOf(TestActors.echoActorProps)
      echo ! StartDimension(50)
      expectMsg(StartDimension(50))
    }
  }

  "A MasterActor" must {
    "receive a StateReceived" in {
      val echo = system.actorOf(TestActors.echoActorProps)
      echo ! StateReceived
      expectMsg(StateReceived)
    }
  }
}
