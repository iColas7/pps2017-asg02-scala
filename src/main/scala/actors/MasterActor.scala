package actors

import actors.MasterActor._
import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import controller.GameController
import scala.collection.mutable.ListBuffer
import scala.util.Random

object MasterActor {

  final case class StartDimension(matrixDimension: Int) extends ActorMessage

  final case class StartCell() extends ActorMessage

  final case class Matrix(matrix: Array[Array[Boolean]]) extends ActorMessage

  final case class StartGame() extends ActorMessage

  final case class CellState(state: Boolean) extends ActorMessage

  final case class StateReceived() extends ActorMessage

  final case class UpdateCell(x: Int,y: Int,state: Boolean) extends ActorMessage

  final case class UpdateMatrix(updatedMatrix: Array[Array[Boolean]]) extends ActorMessage

  final case class GuiUpdated() extends ActorMessage

  final case class StopGame() extends ActorMessage

  final case class RestartGame() extends ActorMessage

  val MASTER_SYSTEM = "MySystem"

  val STATE = "State "

  val ARROW = " -> "

  val ALIVE_CELLS = "Alive cells: "

  val SCORE = "-"

}

class MasterActor(gameController: GameController) extends Actor {

  var cellActorRef: ActorRef = _
  var gameState: Array[Array[Boolean]] = new Array[Array[Boolean]](50)
  var cellActor = new ListBuffer[ActorRef]
  var play = false
  var matrixDimension: Int = 0
  val system: ActorSystem = ActorSystem.create(MASTER_SYSTEM)
  val counterRows = 0
  val counterColumns = 0
  var cellState: Boolean = false
  var cellName: String = _
  var answersFromCellArrived = 0
  var generation = 0


  def receive: PartialFunction[Any,Unit] = {
    case StartDimension(startMatrixDimension) => computeStartDimension(startMatrixDimension)
    case StartGame() => computeStartGame()
    case RestartGame() => computeRestartGame()
    case StateReceived() => computeStateReceived()
    case GuiUpdated() => computeGuiUpdate()
    case UpdateCell(x,y,updatedStateCell) => computeUpdateCell(x,y,updatedStateCell)
    case StopGame() => computeStopGame()
  }

  private def computeStartDimension(matrixDimension: Int): Unit = {
    this.matrixDimension = matrixDimension
    gameState = Array.ofDim[Boolean](matrixDimension,matrixDimension)
    initializeRandomMatrix(matrixDimension)
    gameController.updateGUI(Matrix(gameState))
  }

  private def computeStartGame(): Unit = {
    this.play = true
    for (row <- counterRows until matrixDimension) {
      for (column <- counterColumns until matrixDimension) {
        cellState = gameState(row)(column)
        cellName = constructCellName(row,column)
        cellActorRef = system.actorOf(Props(new CellActor(row,column,system)),cellName)
        cellActorRef ! CellState(cellState)
      }
    }
  }

  private def computeRestartGame(): Unit = {
    play = true
    answersFromCellArrived = 0
    for (cell <- cellActor) {
      cell ! StartCell()
    }
  }

  private def computeStateReceived(): Unit = {
    answersFromCellArrived += 1
    cellActor += sender
    if (answersFromCellArrived == matrixDimension.`²`) {
      for (cellActor <- cellActor) {
        cellActor ! StartCell()
      }
      answersFromCellArrived = 0
      countAliveCells()
    }
  }

  private def computeGuiUpdate(): Unit = {
    for (cell <- cellActor) cell ! StartCell()
  }

  private def computeStopGame(): Unit = {
    play = false
  }

  private def initializeRandomMatrix(dimension: Int): Unit = {
    val random = new Random
    for (row <- counterRows until dimension) {
      for (column <- counterColumns until dimension) {
        gameState(row)(column) = random.nextBoolean
      }
    }
  }

  private def computeUpdateCell(x: Int,y: Int,cellState: Boolean): Unit = {
    answersFromCellArrived += 1
    gameState(x)(y) = cellState
    if (answersFromCellArrived == matrixDimension.`²` & play) {
      gameController.updateGUI(UpdateMatrix(gameState))
      answersFromCellArrived = 0
      countAliveCells()
    }
  }

  private def countAliveCells(): Unit = {
    var aliveCells = 0
    for (row <- counterRows until gameState.length) {
      for (column <- counterColumns until gameState.length) {
        if (gameState(row)(column)) aliveCells += 1
      }
    }
    printStatistics(aliveCells)
  }

  private def printStatistics(aliveCells: Int): Unit = {
    println(STATE + generation + ARROW + ALIVE_CELLS + aliveCells)
    generation += 1
  }

  private def constructCellName(row: Int,column: Int): String = {
    row.toString + SCORE + column.toString
  }

  /**
    * Implicit class to calculate square of int, in my case of matrix's dimension.
    * @param i Integer to calculate his square.
    */
  implicit class PowerInt(i: Int) {
    def `²`: Int = i * i
  }

}
