package actors

import actors.CellActor._
import actors.MasterActor._
import akka.actor.{Actor, ActorRef, ActorSelection, ActorSystem}
import model.MainScala.GAME_SIZE

import scala.collection.mutable.ListBuffer

object CellActor {
  final case class RequestState() extends ActorMessage
  final case class InformNeighbourOfMyState(state: Boolean) extends ActorMessage
  val EXPLORING_NEIGHBOURS = 2
  val CELL_ALIVE_FIRST_CASE = 2
  val CELL_ALIVE_SECOND_CASE = 3
  val AKKA_PATH = "akka://MySystem/user/"
}

class CellActor(x: Int, y: Int, systemFromMaster: ActorSystem) extends Actor {

  var state = false
  var countAliveCells = 0
  var countAnswers = 0
  var neighbours = new ListBuffer[String]
  var neighbourToContact: ActorSelection = _
  val system: ActorSystem = systemFromMaster
  var masterActorRef: ActorRef = _

  override def preStart(): Unit = {
    neighbours(x,y)
  }

  def receive: PartialFunction[Any,Unit] = {
    case CellState(cellState) => computeCellState(cellState,sender)
    case StartCell() => computeStartCell()
    case RequestState() => computeRequestState()
    case InformNeighbourOfMyState(cellState) => computeNeighbour(cellState: Boolean)
  }

  private def computeCellState(state: Boolean,sender: ActorRef): Unit = {
    this.state = state
    masterActorRef = sender
    masterActorRef ! StateReceived()
  }

  private def computeStartCell(): Unit = {
    for (neighbours <- neighbours) {
      neighbourToContact = system.actorSelection(neighbours)
      neighbourToContact ! RequestState()
    }
  }

  private def computeRequestState(): Unit = {
    sender ! InformNeighbourOfMyState(state)
  }

  private def computeNeighbour(cellState: Boolean): Unit = {
    countAnswers -= 1
    if (cellState) countAliveCells += 1
    if (countAnswers == 0) {
      if (state) {
        state = countAliveCells == CELL_ALIVE_FIRST_CASE || countAliveCells == CELL_ALIVE_SECOND_CASE
      } else {
        state = countAliveCells == CELL_ALIVE_SECOND_CASE
      }
      masterActorRef ! UpdateCell(x, y, state)
      countAliveCells = 0
      countAnswers = neighbours.size
    }
  }

  private def neighbours(row: Int,column: Int): Unit = {
    for (counterRows <- row - 1 until row + EXPLORING_NEIGHBOURS) {
      for (counterColumns <- column- 1 until column + EXPLORING_NEIGHBOURS) {
        if (!((counterColumns == column) & (counterRows == row))) {
          if (withinGrid(counterRows, counterColumns)) {
            neighbours += constructNeighbour(counterRows, counterColumns)
            countAnswers += 1
          }
        }
      }
    }
  }

  private def withinGrid(rowNum: Int, columnNum: Int): Boolean = {
    if ((columnNum < 0) || (rowNum < 0)) return false
    if ((columnNum >= GAME_SIZE) || (rowNum >= GAME_SIZE)) return false
    true
  }

  private def constructNeighbour(counterRows: Int, counterColumns: Int): String = {
    AKKA_PATH + counterRows + SCORE + counterColumns
  }

}
