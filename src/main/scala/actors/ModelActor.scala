
package actors

import akka.actor.Actor

/**
  * This trait models a contract between all the actors running on client.
  */
trait ModelActor extends Actor {

  /**
    * Name of the actor.
    *
    * @return the name of the actor.
    */
  def username: String
}

/**
  * Trait to be extended by all the messages received by the client actors.
  */
trait ActorMessage
