package model

import controller.GameController
import view.MainUI


  object MainScala {
    val GAME_SIZE = 100

    def main(args: Array[String]): Unit = {
      val gameController: GameController = new GameController()
      new MainUI(gameController)
      gameController.createMasterActor(GAME_SIZE)

    }

}


