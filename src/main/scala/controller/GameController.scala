package controller

import actors.{ActorMessage,MasterActor}
import actors.MasterActor._
import akka.actor.{ActorRef,ActorSystem,Props}
import view.MainUI
import GameController._


object GameController {
  val UNKNOWN_ERROR: String = "Unknown message received"
  val SYSTEM = "MySystem"
}

class GameController() {

  var masterActorRef: ActorRef = _
  val system = ActorSystem(SYSTEM)
  var javaController: MainUI = _

  /**
    * Method to set object to communicate with Java GUI.
    *
    * @param mainUI Object of MainUI.
    */
  def setJavaController(mainUI: MainUI): Unit = {
    javaController = mainUI
  }

  /**
    * Method to start new game.
    */
  def startGame(): Unit = {
    masterActorRef ! StartGame()
  }

  /**
    * Method to create a new MasterActor and a new game with dimension.
    *
    * @param matrixDimension Matrix's dimension.
    */
  def createMasterActor(matrixDimension: Int): Unit = {
    masterActorRef = system.actorOf(Props(new MasterActor(this)))
    masterActorRef ! StartDimension(matrixDimension)
  }

  /**
    * Method to restart a game.
    */
  def restartGame(): Unit = {
    masterActorRef ! RestartGame()
  }

  /**
    * Method to stop a game.
    */
  def stopGame(): Unit = {
    masterActorRef ! StopGame()
  }

  /**
    * Method to send the matrix to GUI.
    *
    * @param matrix Matrix to send to GUI.
    */
  def sendMatrix(matrix: Array[Array[Boolean]]): Unit = {
    javaController.initializeMatrix(matrix)
  }

  /**
    * Method to send the updated matrix to GUI.
    *
    * @param matrix Updated matrix.
    */
  def updateMatrix(matrix: Array[Array[Boolean]]): Unit = {
    javaController.initializeMatrix(matrix)
    masterActorRef ! GuiUpdated()
  }


  /**
    * Method called from MasterActor to update GUI.
    *
    * @param message Type of message from actor.
    */
  def updateGUI(message: ActorMessage): Unit = message match {
    case Matrix(gameState) => sendMatrix(gameState)
    case UpdateMatrix(gameState) => updateMatrix(gameState)
    case _ => javaController.notifyError(new Throwable(UNKNOWN_ERROR))
  }


}