package model;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class RunnableActionAdapter extends AbstractAction {

  private static final long serialVersionUID = 8416675196778287261L;
  private final Runnable runnable;

  public RunnableActionAdapter(Runnable runnable) {
    this.runnable = runnable;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    runnable.run();
  }
}
