package view;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;

import model.*;

import java.awt.*;
import java.awt.event.*;

public class GameDrawerPanel extends JPanel {

  private static final long serialVersionUID = 1283213170488581829L;
  private static final int GAME_SIZE = MainScala.GAME_SIZE() + 2;
  private static final int MIN_GRID_SIZE = 15;
  private static final int MIN_PIXEL_SIZE = 3;
  private static final int MAX_PIXEL_SIZE = 70;
  private int pixelSize = 10, pixelOffsetX = 0, pixelOffsetY = 0;
  private boolean[][] gameState = new boolean[GAME_SIZE][GAME_SIZE];
  private final JScrollBar horizontalScrollBar, verticalScrollBar;

  GameDrawerPanel(JScrollBar horizontalScrollBar, JScrollBar verticalScrollBar) {
    MouseHandler m = new MouseHandler();
    addMouseListener(m);
    addMouseMotionListener(m);
    addMouseWheelListener(m);
    addComponentListener(new ResizeHandler());
    this.horizontalScrollBar = horizontalScrollBar;
    this.verticalScrollBar = verticalScrollBar;
    getActionMap().put("Scroll Up", new RunnableActionAdapter(() -> scrollRelative(0, -pixelSize)));
    getInputMap().put(KeyStroke.getKeyStroke("UP"), "Scroll Up");
    getActionMap().put("Scroll Down", new RunnableActionAdapter(() -> scrollRelative(0, pixelSize)));
    getInputMap().put(KeyStroke.getKeyStroke("DOWN"), "Scroll Down");
    getActionMap().put("Scroll Left", new RunnableActionAdapter(() -> scrollRelative(-pixelSize, 0)));
    getInputMap().put(KeyStroke.getKeyStroke("LEFT"), "Scroll Left");
    getActionMap().put("Scroll Right", new RunnableActionAdapter(() -> scrollRelative(pixelSize, 0)));
    getInputMap().put(KeyStroke.getKeyStroke("RIGHT"), "Scroll Right");
    getActionMap().put("Page Up", new RunnableActionAdapter(() -> scrollRelative(0, -getHeight())));
    getInputMap().put(KeyStroke.getKeyStroke("PAGE_UP"), "Page Up");
    getActionMap().put("Page Down", new RunnableActionAdapter(() -> scrollRelative(0, getHeight())));
    getInputMap().put(KeyStroke.getKeyStroke("PAGE_DOWN"), "Page Down");
    updateScrollBars();
  }

  private void updateScrollBars() {
    horizontalScrollBar.setValues(pixelOffsetX, getVisibleGridWidth(), 0, GAME_SIZE - getVisibleGridWidth());
    verticalScrollBar.setValues(pixelOffsetY, getVisibleGridHeight(), 0, GAME_SIZE - getVisibleGridHeight());
  }

  @Override
  public Dimension getPreferredSize() {
    return new Dimension(800, 600);
  }

  void initializeMatrix(boolean[][] mat) {
    for (int i = 1; i <= mat.length; i++) {
      for (int j = 1; j <= mat.length; j++) {
        gameState[j][i] = mat[i - 1][j - 1];
      }
    }
    repaint();
  }

  @Override
  protected void paintComponent(Graphics graphics) {
    super.paintComponent(graphics);
    graphics.clearRect(0, 0, getWidth(), getHeight());
    graphics.setColor(Color.GREEN);
    for (int i = -1; i <= getVisibleGridWidth() + 1; i++) {
      for (int j = -1; j <= getVisibleGridHeight() + 1; j++) {
        int curGridX = i + pixelOffsetX / pixelSize;
        int curGridY = j + pixelOffsetY / pixelSize;
        if (curGridX < 0 || curGridY < 0) continue;
        try {
          if (gameState[curGridX + 1][curGridY + 1]) {
            int pixelX = i * pixelSize - pixelOffsetX % pixelSize;
            int pixelY = j * pixelSize - pixelOffsetY % pixelSize;
            if (pixelSize == 1) graphics.drawLine(pixelX, pixelY, pixelX, pixelY);
            else graphics.fillRect(pixelX, pixelY, pixelSize, pixelSize);
          }
        } catch (Exception e) {
        }
      }
    }
    if (pixelSize >= MIN_GRID_SIZE) {
      graphics.setColor(Color.gray);
      for (int i = 0; i <= getVisibleGridWidth() + 1; i++) {
        int pixelX = i * pixelSize - pixelOffsetX % pixelSize;
        graphics.drawLine(pixelX, 0, pixelX, getHeight());
      }
      for (int i = 0; i <= getVisibleGridHeight() + 1; i++) {
        int pixelY = i * pixelSize - pixelOffsetY % pixelSize;
        graphics.drawLine(0, pixelY, getWidth(), pixelY);
      }
    }
  }

  private int getVisibleGridHeight() {
    return getHeight() / pixelSize;
  }

  private int getVisibleGridWidth() {
    return getWidth() / pixelSize;
  }

  private Dimension getScreenGridSize() {
    return new Dimension(getVisibleGridWidth(), getVisibleGridHeight());
  }

  private void scrollRelative(int xAmount, int yAmount) {
    pixelOffsetX = Util.clampInteger(pixelOffsetX + xAmount, 0, GAME_SIZE - getScreenGridSize().width);
    pixelOffsetY = Util.clampInteger(pixelOffsetY + yAmount, 0, GAME_SIZE - getScreenGridSize().height);
    updateScrollBars();
    repaint();
  }

  void scrollXAbsolute(int x) {
    pixelOffsetX = x;
    repaint();
  }

  void scrollYAbsolute(int y) {
    pixelOffsetY = y;
    repaint();
  }

  private void setZoom(int newSize) {
    pixelSize = Util.clampInteger(newSize, MIN_PIXEL_SIZE, MAX_PIXEL_SIZE);
  }

  private class ResizeHandler extends ComponentAdapter {
    @Override
    public void componentResized(ComponentEvent componentEvent) {
      updateScrollBars();
    }
  }

  private class MouseHandler extends MouseInputAdapter {
    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
      int newSize = pixelSize + e.getWheelRotation();
      setZoom(newSize);
      updateScrollBars();
      repaint();
    }
  }
}
