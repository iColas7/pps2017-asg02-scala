package view;

import javax.swing.*;
import java.awt.*;

class GameOfLifePanel extends JPanel {

  private static final long serialVersionUID = 4952183783935394607L;
  private final GameDrawerPanel gameDrawerPanel;

  GameOfLifePanel() {
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.weightx = 1;
    c.weighty = 1;
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;
    c.fill = GridBagConstraints.BOTH;
    JScrollBar verticalScrollBar = new JScrollBar(Adjustable.VERTICAL);
    JScrollBar horizontalScrollBar = new JScrollBar(Adjustable.HORIZONTAL);
    gameDrawerPanel = new GameDrawerPanel(horizontalScrollBar, verticalScrollBar);
    horizontalScrollBar.addAdjustmentListener(e -> gameDrawerPanel.scrollXAbsolute(e.getValue()));
    verticalScrollBar.addAdjustmentListener(e -> gameDrawerPanel.scrollYAbsolute(e.getValue()));
    add(gameDrawerPanel, c);
    c.weighty = 0;
    c.weightx = 0;
    c.gridx = 1;
    c.gridwidth = GridBagConstraints.RELATIVE;
    c.fill = GridBagConstraints.VERTICAL;
    add(verticalScrollBar, c);
    c.gridx = 0;
    c.gridy = 1;
    c.gridwidth = 1;
    c.gridheight = GridBagConstraints.RELATIVE;
    c.fill = GridBagConstraints.HORIZONTAL;
    add(horizontalScrollBar, c);
  }

  void init(boolean[][] mat) {
    gameDrawerPanel.initializeMatrix(mat);
  }
}
