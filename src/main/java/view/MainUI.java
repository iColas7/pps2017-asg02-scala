package view;

import controller.GameController;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;

public class MainUI {

  private static final String PLAY_TEXT = "Play";
  private static final String STOP_TEXT = "Stop";
  private static final String TITLE = "Game of Life";
  private final JButton playButton, stopButton;
  private GameOfLifePanel gamePanel;
  private GameController gameController;
  private int countPlayTimes;

  public MainUI(final GameController gameController) {
    JPanel buttonPanel = new JPanel();
    JFrame frame = new JFrame();
    BoxLayout box = new BoxLayout(buttonPanel, BoxLayout.X_AXIS);
    this.gameController = gameController;
    this.gameController.setJavaController(this);
    this.countPlayTimes = 0;
    this.gamePanel = new GameOfLifePanel();
    this.playButton = new JButton(PLAY_TEXT);
    this.playButton.addActionListener(this::play);
    this.stopButton = new JButton(STOP_TEXT);
    this.stopButton.addActionListener(this::stop);
    this.stopButton.setEnabled(false);
    frame.setTitle(TITLE);
    frame.setLayout(new BorderLayout());
    frame.add(gamePanel);
    buttonPanel.setLayout(box);
    buttonPanel.add(playButton);
    buttonPanel.add(stopButton);
    frame.add(buttonPanel, BorderLayout.NORTH);
    frame.pack();
    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    frame.setVisible(true);
    frame.setLocationRelativeTo(null);
  }

  private void play(ActionEvent e) {
    this.countPlayTimes++;
    if (countPlayTimes == 1) {
      this.gameController.startGame();
    } else {
      this.gameController.restartGame();
    }
    this.stopButton.setEnabled(true);
    this.playButton.setEnabled(false);
  }

  private void stop(ActionEvent e) {
    this.gameController.stopGame();
    this.stopButton.setEnabled(false);
    this.playButton.setEnabled(true);
  }

  public void initializeMatrix(boolean[][] gameState) {
    this.gamePanel.init(gameState);
  }

  public void notifyError(Throwable throwable) {
    System.out.println(throwable.getMessage());
  }

}
